defmodule ColmenaWeb.PdbView do
  use ColmenaWeb, :view

  def render("pdb_download_validation.json", _) do
    %{
      error: %{
        pdbId: "Must be a not empty string",
        taskType: "Must be either structure or surface"
      }
    }
  end

  def render("surface_validation.json", _) do
    %{
      error: %{
        pdbSurfaceId: "Must be a number query param"
      }
    }
  end

  def render("structure_validation.json", _) do
    %{
      error: %{
        pdbStructureId: "Must be a number query param"
      }
    }
  end
end
