defmodule ColmenaWeb.RunnerView do
  use ColmenaWeb, :view

  alias ColmenaWeb.JobView
  alias ColmenaWeb.DateView

  def render("index.json", %{jobs: jobs}) do
    %{data: render_many(jobs, JobView, "job.json")}
  end

  def render("show.json", %{job: job}) do
    %{data: render_one(job, JobView, "job.json")}
  end

  def render("job.json", %{job: job}) do
    %{
      id: job.id,
      state: job.state,
      name: job.name,
      result: job.result,
      startedAt: DateView.render("date.json", job.started_at),
      finishedAt: DateView.render("date.json", job.finished_at)
    }
  end

  def render("params_validation.json", _) do
    %{
      error: %{
        pdbId: "Must a be not blank string",
        name: "Must be a not blank string",
        tasks: "Must be a map of {task: name, subtasks:[]}"
      }
    }
  end

  def render("tasks_validation.json", %{task_tree: valid_task_tree}) do
    %{
      error: %{
        tasks: %{
          msg: "the task list must be a path from the valid task tree",
          validTaskTree: task_tree_to_map(valid_task_tree)
        }
      }
    }
  end

  defp task_tree_to_map(valid_task_tree) do
    Enum.reduce(valid_task_tree, [], fn {node, tree}, xs -> [%{node => task_tree_to_map(tree)} | xs] end)
  end

end
