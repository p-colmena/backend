defmodule ColmenaWeb.DateView do

  @spec render(String.t(), nil) :: %{present: boolean, year: integer, month: integer, day: integer}
  def render("date.json", nil) do
    %{
      present: false
    }
  end

  @spec render(String.t(), NaiveDateTime) :: %{present: boolean, year: integer, month: integer, day: integer}
  def render("date.json", date) do
    %{
      present: true,
      year: date.year,
      month: date.month,
      day: date.day
    }
  end
end
