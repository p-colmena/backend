defmodule ColmenaWeb.JobView do
  use ColmenaWeb, :view

  alias ColmenaWeb.JobView

  def render("index.json", %{jobs: jobs}) do
    %{
      jobs: render_many(jobs, JobView, "job.json")
    }
  end

  def render("show.json", %{job: job}) do
    %{data: render_one(job, JobView, "job.json")}
  end

  def render("job.json", %{job: job}) do
    %{
      id: job.id,
      state: job.state,
      pdbId: job.pdb_id,
      name: job.name,
      result: job.result,
      duration: duration(job)
    }
  end

  def duration(job) do
    if(job.finished_at != nil) do
      DateTime.diff(job.finished_at, job.started_at)
    else
      nil
    end
  end
end
