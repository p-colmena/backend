defmodule ColmenaWeb.TaskView do
  use ColmenaWeb, :view

  alias Colmena.Task.TaskPdbStructurue

  def render("task_validation.json", _) do
    %{
      error: %{
        jobId: "Must be a number query param"
      }
    }  end

  @spec render(String.t(), TaskPdbStructurue) :: %{}
  def render("task_pdb_structure.json", task) do
    %{
      id: task.id,
      jobId: task.job.id,
      pdbStructureId: task.pdb_structure.id,
      pdbId: task.pdb_structure.pdb_id
    }
  end

  def render("task_pdb_surface.json", task) do
    %{
      id: task.id,
      taskPdbStructureId: task.task_pdb_structure.id,
      pdbSurfaceId: task.pdb_surface.id,
      pdbId: task.pdb_surface.pdb_id
    }
  end

  def render("task_pdb_pockets.json", task) do
    %{
      id: task.id,
      taskPdbStructureId: task.task_pdb_structure.id,
      pdbPocketsId: task.pdb_pockets.id,
      pockets: map_pockets(task.pdb_pockets.pockets),
      pdbId: task.pdb_pockets.pdb_id,
      poketsInfo: task.pdb_pockets.info
    }
  end

  def render("tasks.json", tasks) do
    %{
      tasks: Enum.map(tasks.tasks, fn task -> map_task(task) end)
    }
  end

  defp map_pockets(pockets) do
    Enum.map(pockets, fn pocket -> %{taskType: "pocket", id: pocket.id, pdb_id: pocket.pdb_id} end)
  end


  defp map_task(task) do
    case task do
      {:structure, task_structure} ->
        Map.put(render("task_pdb_structure.json", task_structure), "taskType", "structure")
      {:surface, task_surface} ->
        Map.put(render("task_pdb_surface.json", task_surface), "taskType", "surface")
      {:pockets, task_pockets} ->
        Map.put(render("task_pdb_pockets.json", task_pockets), "taskType", "pockets")
    end
  end
end
