defmodule ColmenaWeb.TaskController do
  use ColmenaWeb, :controller

  action_fallback ColmenaWeb.FallbackController
  alias Colmena.Task

  defp validate(params) do
    with %{"pdbId" => pdb_id} <- params do
      {:ok, pdb_id}
    else
      _ -> {:error, {:bad_request, :tasks}}
    end
  end

  def get_tasks(conn, params) do
    with {:ok, pdb_id} <- validate(params),
         {:ok, tasks} <- Task.show_tasks(pdb_id) do
      render(conn, "tasks.json", %{tasks: tasks})
    end
  end

end
