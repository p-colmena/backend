defmodule ColmenaWeb.RunnerController do
  use ColmenaWeb, :controller

  alias Colmena.Runner
  alias Colmena.Process

  action_fallback ColmenaWeb.FallbackController

  def validate(params) do
    with %{"pdbId" => pdb_id, "name" => name, "tasks" => tasks} <- params do
      if(is_binary(pdb_id) && is_binary(name) && is_map(tasks)) do
          {:ok, {pdb_id, name, tasks}}
      else
        {:error, {:validation, :run_params}}
      end
    else
      _ -> {:error, {:bad_request, :run_params}}
    end
  end

  def run(conn, params) do
    with {:ok, {pdb_id, name, tasks_params}} <- validate(params),
         :ok <- Runner.validate(tasks_params),
         tasks <- Runner.to_valid_task_tree(tasks_params),
         func <- fn job -> Runner.run_tasks(tasks, %{job: job, pdb_id: pdb_id}) end,
         {:ok, job} <- Process.run_task(pdb_id, name, func) do
      render(conn, "show.json", job: job)
    end
  end
end
