defmodule ColmenaWeb.JobController do
  use ColmenaWeb, :controller

  alias Colmena.Process
  alias ColmenaWeb.ListParamParser

  action_fallback ColmenaWeb.FallbackController

  def index(conn, params) do
    {search_term} = ListParamParser.build_paging_info(params)
    IO.inspect(search_term)
    render(
      conn,
      "index.json",
      jobs: Process.list_jobs(search_term),
    )
  end
end
