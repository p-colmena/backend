defmodule ColmenaWeb.PdbController do
  use ColmenaWeb, :controller

  action_fallback ColmenaWeb.FallbackController
  alias Colmena.PdbProcessor

  defp validate_pdb(params) do
    with %{"taskType" => task_type, "pdbId" => pdb_id} <- params,
         true <- task_type == "structure" || task_type == "surface" || task_type == "pockets",
         true <- String.length(pdb_id) > 0 do
      {:ok, {task_type, pdb_id}}
    else
      _ -> {:error, {:bad_request, :download_pdb}}
    end
  end

  defp validate_pdb_structure_id(params) do
    with %{"pdbStructureId" => pdb_structure_id} <- params do
      try do
        {:ok, String.to_integer(pdb_structure_id)}
      rescue
        _ in ArgumentError -> {:error, {:bad_request, :download_pdb_structure}}
      end
    else
      _ -> {:error, {:bad_request, :download_pdb_structure}}
    end
  end

  defp validate_pdb_surface_id(params) do
    with %{"pdbSurfaceId" => pdb_surface_id} <- params do
      try do
        {:ok, String.to_integer(pdb_surface_id)}
      rescue
        _ in ArgumentError -> {:error, {:bad_request, :download_pdb_structure}}
      end
    else
      _ -> {:error, {:bad_request, :download_pdb_surface}}
    end
  end

  defp validate_pdb_pockets_id(params) do
    with %{"pdbPocketsId" => pdb_pockets_id} <- params do
      try do
        {:ok, String.to_integer(pdb_pockets_id)}
      rescue
        _ in ArgumentError -> {:error, {:bad_request, :download_pdb_pockets}}
      end
    else
      _ -> {:error, {:bad_request, :download_pdb_pockets}}
    end
  end

  defp validate_pocket_id(params) do
    with %{"id" => pocket_id} <- params do
      try do
        {:ok, String.to_integer(pocket_id)}
      rescue
        _ in ArgumentError -> {:error, {:bad_request, :download_pocket}}
      end
    else
      _ -> {:error, {:bad_request, :download_pocket}}
    end
  end

  def download_pdb(conn, params) do
    with {:ok, {task_type, pdb_id}} <- validate_pdb(params),
         {:ok, pdb} <- pdb_by_type(task_type, pdb_id) do
      download conn, pdb, pdb_id
    end
  end

  def pdb_by_type(task_type, pdb_id) do
    case task_type do
      "structure" -> PdbProcessor.show_pdb_structure_by_pdb_id(pdb_id)
      "surface" -> PdbProcessor.show_pdb_surface_by_pdb_id(pdb_id)
      "pockets" -> PdbProcessor.show_pdb_pockets_by_pdb_id(pdb_id)
    end
  end

  def download_pdb_structure(conn, params) do
    with {:ok, pdb_structure_id} <- validate_pdb_structure_id(params),
         {:ok, pdb_structure} <- PdbProcessor.show_pdb_structure(pdb_structure_id) do
      download conn, pdb_structure.structure, pdb_structure.pdb_id <> ".pdb"
    end
  end

  def download_pdb_surface(conn, params) do
    with {:ok, pdb_surface_id} <- validate_pdb_surface_id(params),
         {:ok, pdb_surface} <- PdbProcessor.show_pdb_surface(pdb_surface_id) do
      download conn, pdb_surface.structure, "surface_" <> pdb_surface.pdb_id <> ".pdb"
    end
  end

  def download_pdb_pockets_result(conn, params) do
    with {:ok, pdb_pockets_id} <- validate_pdb_pockets_id(params),
         {:ok, pdb_pockets_result} <- PdbProcessor.show_pdb_pockets(pdb_pockets_id) do
      download conn, pdb_pockets_result.zip, "pockets_" <> pdb_pockets_result.pdb_id <> ".zip"
    end
  end

  def download_pdb_pockets_pdb(conn, params) do
    with {:ok, pdb_pockets_id} <- validate_pdb_pockets_id(params),
         {:ok, pdb_pockets_result} <- PdbProcessor.show_pdb_pockets(pdb_pockets_id) do
      download conn, pdb_pockets_result.pdb, "pockets_" <> pdb_pockets_result.pdb_id <> ".pdb"
    end
  end

  def dowload_pocket(conn, params) do
    with {:ok, pocket_id} <- validate_pocket_id(params),
         {:ok, pocket} <- PdbProcessor.show_pocket(pocket_id) do
      download conn, pocket.structure, "pocket_" <> pocket.pdb_id <> ".pdb"
    end
  end

  defp download(conn, structure, name) do
    send_download(conn, {:binary, structure}, filename: name)
  end
end
