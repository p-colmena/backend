defmodule ColmenaWeb.FallbackController do
  use ColmenaWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(ColmenaWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(ColmenaWeb.ErrorView, :"404.json")
  end

  def call(conn, {:error, {:validation, :run_params}}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(ColmenaWeb.RunnerView, "params_validation.json")
  end

  def call(conn, {:error, {:bad_request, :run_params}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.RunnerView, "params_validation.json")
  end

  def call(conn, {:error, {:validation, :tasks, valid_task_tree}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.RunnerView, "tasks_validation.json", task_tree: valid_task_tree)
  end

  def call(conn, {:error, {:bad_request, :download_pdb_surface}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.PdbView, "surface_validation.json")
  end

  def call(conn, {:error, {:bad_request, :download_pdb_structure}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.PdbView, "structure_validation.json")
  end

  def call(conn, {:error, {:bad_request, :download_pdb}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.PdbView, "pdb_download_validation.json")
  end

  def call(conn, {:error, {:bad_request, :tasks}}) do
    conn
    |> put_status(:bad_request)
    |> render(ColmenaWeb.TaskView, "task_validation.json")
  end
end
