defmodule ColmenaWeb.Router do
  use ColmenaWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(CORSPlug, origin: ["http://localhost:3000", "http://127.0.0.1:3000"])
  end

  scope "/api", ColmenaWeb do
    pipe_through(:api)

    options("/jobs", JobController, :options)
    get("/jobs", JobController, :index)
    post("/jobs", JobController, :create)
    get("/jobs/get-result", JobController, :get_result)

    options("/run", RunnerController, :options)
    post("/run", RunnerController, :run)

    options("/task", TaskController, :options)
    get("/task/all", TaskController, :get_tasks)

    options("/pdb", PdbController, :options)
    get("/pdb/structure", PdbController, :download_pdb_structure)
    get("/pdb/surface", PdbController, :download_pdb_surface)

    get("/pdb/pockets/display", PdbController, :download_pdb_pockets_pdb)
    get("/pdb/pockets", PdbController, :download_pdb_pockets_result)

    get("/pdb/pocket", PdbController, :dowload_pocket)

    get("/pdb/download", PdbController, :download_pdb)
  end
end
