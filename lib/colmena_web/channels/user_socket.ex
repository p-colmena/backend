defmodule ColmenaWeb.UserSocket do
  use Phoenix.Socket

  channel "jobs:*", ColmenaWeb.JobsChannel

  transport :websocket, Phoenix.Transports.WebSocket

  def connect(_params, socket) do
    {:ok, socket}
  end

  def id(_socket), do: nil
#  def id(socket), do: "user_socket:#{socket.assigns.user_id}"
end
