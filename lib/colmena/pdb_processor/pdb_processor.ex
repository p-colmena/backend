defmodule Colmena.PdbProcessor do
  alias Colmena.PdbProcessor.Http
  alias Colmena.PdbProcessor.PdbStructure
  alias Colmena.PdbProcessor.PdbSurface
  alias Colmena.PdbProcessor.Pocket
  alias Colmena.PdbProcessor.PdbPockets
  alias Colmena.PdbProcessor.PdbSurfaceCalculator
  alias Colmena.PdbProcessor.PdbPocketFinder
  alias Colmena.Repo

  import Ecto.Query, warn: false
  require Logger

  # Show things
  ###############################################################################

  @spec show_pdb_structure_by_pdb_id(Integer) :: {:ok, binary()} | {:error, any()}
  def show_pdb_structure_by_pdb_id(pdb_id) do
    case Repo.get_by(PdbStructure, pdb_id: pdb_id) do
      nil -> {:error, :not_found}
      structure -> {:ok, structure.structure}
    end
  end

  @spec show_pdb_surface_by_pdb_id(Integer) :: {:ok, binary()} | {:error, any()}
  def show_pdb_surface_by_pdb_id(pdb_id) do
    case Repo.get_by(PdbSurface, pdb_id: pdb_id) do
      nil -> {:error, :not_found}
      surface -> {:ok, surface.structure}
    end
  end

  @spec show_pdb_pockets_by_pdb_id(Integer) :: {:ok, binary()} | {:error, any()}
  def show_pdb_pockets_by_pdb_id(pdb_id) do
    case Repo.get_by(PdbPockets, pdb_id: pdb_id) do
      nil -> {:error, :not_found}
      pdb_pockets -> {:ok, pdb_pockets.pdb}
    end
  end

  @spec show_pdb_pockets(Integer) :: {:ok, binary()} | {:error, any()}
  def show_pdb_pockets(id)do
    case Repo.get(PdbPockets, id) do
      nil -> {:error, :not_found}
      pdb_pockets -> {:ok, pdb_pockets}
    end
  end

  @spec show_pdb_structure(Integer) :: {:ok, PdbStructure} | {:error, any()}
  def show_pdb_structure(id) do
    case Repo.get(PdbStructure, id) do
      nil -> {:error, :not_found}
      structure -> {:ok, structure}
    end
  end

  @spec show_pdb_surface(Integer) :: {:ok, PdbSurface} | {:error, any()}
  def show_pdb_surface(id) do
    case Repo.get(PdbSurface, id) do
      nil -> {:error, :not_found}
      surface -> {:ok, surface}
    end
  end

  @spec show_pocket(Integer) :: {:ok, Pocket} | {:error, any()}
  def show_pocket(id) do
    case Repo.get(Pocket, id) do
      nil -> {:error, :not_found}
      pocket -> {:ok, pocket}
    end
  end

  # Create things
  ###############################################################################

  def create_pocket(pdb_pockets, content) do
    %Pocket{}
    |> Pocket.create_changeset(%{pdb_pockets_id: pdb_pockets.id, pdb_id: pdb_pockets.pdb_id, structure: content})
    |> Repo.insert
  end

  def create_pdb_structure(attrs \\ %{}) do
    Repo.insert(PdbStructure.create_pdb_structure_changeset(%PdbStructure{}, attrs))
  end

  def create_pdb_surface(attrs \\ %{}) do
    Repo.insert(PdbSurface.create_changeset(%PdbSurface{}, attrs))
  end

  def create_pdb_pockets(pdb_structure, info, pdb, zip) do
    %PdbPockets{}
    |> PdbPockets.create_changeset(%{pdb_id: pdb_structure.pdb_id, info: info, pdb: pdb, zip: zip})
    |> Repo.insert
  end

  @spec save(String.t()) :: PdbStructure | {:error, any()}
  def save(pdb_id) do
    Logger.info("Saving pdb #{pdb_id}")
    file_name = pdb_id <> ".pdb"

    with nil <- Repo.get_by(PdbStructure, pdb_id: pdb_id),
         {:ok, body} <- Http.get_pdb(file_name) do
      Logger.info("Pdb structure obtained from service")
      create_pdb_structure(%{pdb_id: pdb_id, structure: body})
    else
      %PdbStructure{} = structure -> {:ok, structure}
      err -> err
    end
  end

  @spec surface(PdbStructure) :: {:ok, PdbSurface} | {:error, any()}
  def surface(pdb_structure) do
    with nil <- Repo.get_by(PdbSurface, pdb_id: pdb_structure.pdb_id),
         {:ok, surface} <- PdbSurfaceCalculator.measure(pdb_structure.structure) do
      create_pdb_surface(%{pdb_id: pdb_structure.pdb_id, structure: surface})
    else
      %PdbSurface{} = surface -> {:ok, surface}
      err -> err
    end
  end

  @spec pockets(PdbStructure) :: {:ok, PdbPockets} | {:error, any()}
  def pockets(pdb_structure) do
    with nil <- Repo.get_by(PdbPockets, pdb_id: pdb_structure.pdb_id),
         {:ok, {info, pdb, pockets_contents, zip}} <- PdbPocketFinder.find_in(pdb_structure.structure),
         {:ok, pdb_pockets} <- create_pdb_pockets(pdb_structure, info, pdb, zip),
         pockets <- Enum.map(pockets_contents, fn content -> create_pocket(pdb_pockets, content) end),
         nil <- Enum.find(pockets, fn pocket -> match? {:error, _}, pocket end)do
      {:ok, %{pdb_pockets | pockets: pockets}}
    else
      %PdbPockets{} = pdb_pockets -> {:ok, Repo.preload(pdb_pockets, [:pockets])}
      true -> {:error, :creating_pockets}
      err -> err
    end
  end
end
