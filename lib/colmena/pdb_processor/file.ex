defmodule Colmena.PdbProcessor.File do
  def save_file(path, file_name, body) do
    full_path = base_path() <> path

    File.mkdir_p(full_path)
    File.write(full_path <> "/" <> file_name, body, [:write])
  end

  @spec save_file(String.t()) :: {:ok, String.t()} | {:error, File.posix()}
  def save_file(body) do
    with filename <- base_path() <> "/" <> UUID.uuid4() <> ".pdb",
         :ok <- File.write(filename, body, [:write]) do
      {:ok, filename}
    end
  end

  @spec split_path_and_name(String.t()) :: {String.t(), String.t()}
  def split_path_and_name(path_and_name) do
    with base <- Path.dirname(path_and_name),
         name <- Path.basename(path_and_name, ".pdb") do
      {base, name}
    end
  end

  @spec get_file_content(String.t()) :: {:ok, String.t()} | {:error, File.posix()}
  def get_file_content(file) do
    File.read(file)
  end

  @spec get_files_contents(String.t()) :: {:ok, [String.t()]} | {:error, File.posix()}
  def get_files_contents(path) do
    with files <- Path.wildcard(path),
         maybe_contents <- Enum.map(files, fn file -> File.read(file) end),
         nil <- Enum.find(maybe_contents, fn maybe_content -> match? {:error, _}, maybe_content end),
         contents <- Enum.map(maybe_contents, fn {:ok, content} -> content end)
      do
      {:ok, contents}
    else
      true -> {:error, {:loading_files, path}}
    end
  end

  @spec zip(String.t()) :: {:ok, binary()} | {:error, any()}
  def zip(path) do
    with paths <- Path.wildcard(path <> "/**/*.*"),
         maybe_files <- Enum.map(
           paths,
           fn filePath -> {String.to_charlist(Path.basename(filePath)), File.read(filePath)} end
         ),
         nil <- Enum.find(maybe_files, fn maybe_file -> match? {:error, _}, maybe_file end),
         files <- Enum.map(maybe_files, fn {name, {:ok, file}} -> {name, file} end),
         {:ok, {_, binary}} <- :zip.create("zipped", files, [:memory])
      do
      {:ok, binary}
    else
      true -> {:error, {:loading_files, path}}
      any -> any
    end
  end

  defp base_path do
    Path.expand(Application.get_env(:colmena, Colmena.PDB)[:base_path])
  end
end
