defmodule Colmena.PdbProcessor.PdbStructure do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pdb_structures" do
    field :pdb_id, :string
    field :structure, :binary
    timestamps()
  end

  def create_pdb_structure_changeset(pdb_structure, attrs) do
    pdb_structure
    |> cast(attrs, [:pdb_id, :structure])
    |> validate_required([:pdb_id, :structure])
    |> unique_constraint(:pdb_id)
  end
end
