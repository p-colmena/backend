defmodule Colmena.PdbProcessor.PdbSurfaceCalculator do
  alias Colmena.PdbProcessor.File

  def measure(structrure) do
    with {:ok, filename} <- File.save_file(structrure) do
      call_freesasa filename
    end
  end

  defp call_freesasa(filename) do
    case System.cmd "freesasa", ["--format=pdb", filename] do
      {surface, 0} -> {:ok, surface}
      {error, code} -> {:error, :error_mesuring_surface, error, code}
      _ -> {:error, :error_mesuring_surface}
    end
  end
end
