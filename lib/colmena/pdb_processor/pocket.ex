defmodule Colmena.PdbProcessor.Pocket do
  use Ecto.Schema
  import Ecto.Changeset
  alias Colmena.PdbProcessor.PdbPockets

  schema "pockets" do
    field :pdb_id, :string
    field :structure, :binary
    belongs_to :pdb_pockets, PdbPockets
    timestamps()
  end

  def create_changeset(pocket, attrs) do
    pocket
    |> cast(attrs, [:pdb_id, :structure, :pdb_pockets_id])
    |> assoc_constraint(:pdb_pockets)
    |> validate_required([:pdb_id, :structure, :pdb_pockets_id])
  end
end
