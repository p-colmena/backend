defmodule Colmena.PdbProcessor.PdbSurface do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pdb_surfaces" do
    field :pdb_id, :string
    field :structure, :binary
    timestamps()
  end

  def create_changeset(pdb_surface, attrs) do
    pdb_surface
    |> cast(attrs, [:pdb_id, :structure])
    |> validate_required([:pdb_id, :structure])
    |> unique_constraint(:pdb_id)
  end
end
