defmodule Colmena.PdbProcessor.PdbPocketFinder do
  alias Colmena.PdbProcessor.File

  def find_in(structrure) do
    with {:ok, filename} <- File.save_file(structrure) do
      call_fpocket filename
    end
  end

  defp call_fpocket(filename) do
    with {_, 0} <- System.cmd("fpocket", ["-f", filename]),
         {path, name} <- File.split_path_and_name(filename),
         out_dir <- path <> "/" <> name <> "_out",
         {:ok, info_content} <- File.get_file_content(out_dir <> "/" <> name <> "_info.txt"),
         {:ok, info_pdb} <- File.get_file_content(out_dir <> "/" <> name <> "_out.pdb"),
         {:ok, pockets} <- File.get_files_contents(out_dir <> "/pockets/*.pdb"),
         {:ok, binary} <- File.zip(out_dir)
      do
      {:ok, {info_content, info_pdb, pockets, binary}}
    else
      _ -> {:error, :error_finding_pockets}
    end
  end
end
