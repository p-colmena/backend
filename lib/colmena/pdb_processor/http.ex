
defmodule Colmena.PdbProcessor.Http do
  require Logger
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://files.rcsb.org/view"
  plug Tesla.Middleware.RequestLogger
  plug Tesla.Middleware.ResponseLogger

  def get_pdb(fileName) do
    with {:ok, response} <- get("/" <> fileName),
         200 <- response.status do
      {:ok, response.body}
    else
      x when is_integer(x) -> {:error, :response_status_not_ok, x}
      _ -> {:error, :request_failed}
    end
  end
end

defmodule Tesla.Middleware.RequestLogger do
  require Logger

  @behaviour Tesla.Middleware
  def call(env, next, _) do
    Logger.info("Request to: " <> env.url)
    Tesla.run(env, next)
  end
end

defmodule Tesla.Middleware.ResponseLogger do
  require Logger

  @behaviour Tesla.Middleware
  def call(env, next, _) do
    res = Tesla.run(env, next)

    case res do
      {:ok, response} ->
        Logger.info("Response url: " <> response.url)
        Logger.info("Response status: " <> to_string(response.status))
    end

    res
  end
end

