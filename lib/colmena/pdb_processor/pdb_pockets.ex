defmodule Colmena.PdbProcessor.PdbPockets do
  use Ecto.Schema
  import Ecto.Changeset
  alias Colmena.PdbProcessor.Pocket

  schema "pdb_pockets" do
    field :pdb_id, :string
    field :info, :binary
    field :pdb, :binary
    field :zip, :binary
    has_many :pockets, Pocket
    timestamps()
  end

  def create_changeset(pdb_pockets, attrs) do
    pdb_pockets
    |> cast(attrs, [:pdb_id, :info, :pdb, :zip])
    |> validate_required([:pdb_id, :info, :pdb, :zip])
    |> unique_constraint(:pdb_id)
  end
end
