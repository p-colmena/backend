defmodule Colmena.Runner do
  alias Colmena.Task, as: ColmenaTask

  @spec run_tasks(TaskTree, %{}) :: :ok | {:error, any()}
  def run_tasks(tasks, params) do
    with {:ok, res} <- tasks.function.(params) do
      tasks.subtasks
      |> Enum.map(fn s -> Task.async(fn -> run_tasks(s, %{tasks.name => res}) end) end)
      |> Enum.map(fn a -> Task.await a end)
      |> Enum.reduce(:ok, fn x, acc -> if (match? {:error, _}, acc), do: acc, else: x end)
    end
  end

  def to_valid_task_tree(requested) do
    to_valid_task_tree(requested, valid_task_tree())
  end

  defp to_valid_task_tree(requested, valid) do
    TaskTree.node(
      valid.name,
      valid.function,
      Enum.map(
        Map.get(requested, "subtasks"),
        fn r -> to_valid_task_tree r, (Enum.find(valid.subtasks, fn v -> v.name == Map.get(r, "task") end)) end
      )
    )
  end

  def validate(tasks) do
    if (validate(tasks, valid_task_tree())), do: :ok, else: {:error, {:validation, :tasks, valid_task_tree()}}
  end

  defp validate(requested, valid) do
    (Map.get(requested, "task") == valid.name) &&
      (Map.get(requested, "subtasks")
       |> Enum.map(fn r -> List.foldl(valid.subtasks, false, fn v, acc -> validate(r, v) || acc end) end)
       |> Enum.find(true, fn x -> x end))
  end

  def valid_task_tree do
    TaskTree.node(
      "structure",
      fn (%{job: job, pdb_id: pdb_id}) -> ColmenaTask.pdb_structure(job, pdb_id) end,
      [
        TaskTree.node(
          "surface",
          fn %{"structure" => task_pdb_structure} -> ColmenaTask.pdb_surface task_pdb_structure end
        ),
        TaskTree.node(
          "pockets",
          fn %{"structure" => task_pdb_structure} -> ColmenaTask.pdb_pockets task_pdb_structure end
        )
      ]
    )
  end
end

defmodule TaskTree do
  defstruct [:name, :function, :subtasks]

  def node(name, function, subtasks \\ []) do
    %TaskTree{name: name, function: function, subtasks: subtasks}
  end
end