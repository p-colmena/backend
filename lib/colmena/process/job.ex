defmodule Colmena.Process.Job do
  use Ecto.Schema
  import Ecto.Changeset

  def running do
    "running"
  end

  def success do
    "success"
  end

  def error do
    "error"
  end

  @spec is_running(Job) :: boolean
  def is_running(job) do
   running() == job.state
  end

  @spec finished_with_success(Job) :: boolean
  def finished_with_success(job) do
    success() == job.state
  end

  @spec finished_with_success(Job) :: boolean
  def finished_with_error(job) do
    error() == job.state
  end

  schema "jobs" do
    field :state, :string, default: "running"
    field :name, :string
    field :pdb_id, :string
    field :started_at, :utc_datetime_usec, default: DateTime.utc_now()
    field :finished_at, :utc_datetime_usec
    field :result, :string
  end

  # THIS CHANGESET IS ONLY FOR THE ASSOCIATIONS
  def changeset(job, attrs \\ %{}) do
    job
    |> cast(attrs, [:id, :name, :pdb_id, :state, :started_at])
    |> validate_required([:id, :name, :pdb_id, :state, :started_at])
 end

  def create_job_changeset(job, attrs) do
    job
    |> cast(attrs, [:name, :pdb_id])
    |> validate_required([:name, :pdb_id])
  end

  def update_job_changeset(job, state, finished_at, result) do
    job
    |> change(state: state, finished_at: finished_at, result: result)
    |> validate_required([:name, :state, :started_at, :id])
  end
end
