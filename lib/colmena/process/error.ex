defmodule Colmena.Process.Error do
  require Logger

  def map(error) do
    Logger.error("Error processing job: #{inspect(error)}")
    {:current_stacktrace, stacktrace} = Process.info(self(), :current_stacktrace)
    Enum.each(stacktrace, fn (e) -> Logger.error(inspect(e)) end)

    case error do
      {:error, :response_status_not_ok, x} ->
        "Could't get pdb structure, the service response with #{to_string(x)}"
      {:error, :request_failed} ->
        "Could't get pdb structure, the request failed"
      {:error, :error_mesuring_surface, error, code} ->
        "Error measuring surface with error code #{to_string(code)} and response #{to_string(error)}"
      {:error, :error_mesuring_surface} ->
        "Error measuring surface, please contact garciasmithagustin@gmail.com"
      _ ->
        "Unexpected error, please contact garciasmithagustin@gmail.com"
    end
  end
end
