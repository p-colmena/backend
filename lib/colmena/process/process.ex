defmodule Colmena.Process do

  import Ecto.Query, warn: false
  alias Colmena.Repo
  alias Colmena.Process.Job
  alias Colmena.Process.Error, as: RunError
  require Logger

  def deprecated_list_jobs do
    Job
    |> order_by(desc: :started_at)
    |> Repo.all()
  end
  
  def list_jobs(search_term) do
    query = from j in Job, order_by: [desc: :started_at]
    query = add_filter(query, search_term)
    IO.inspect(query)
    Repo.all(query, limit: 100)
  end

  defp add_filter(query, search_term) when search_term == nil or search_term == "", do: query
  defp add_filter(query, original_search_term) do
    search_term = "#{original_search_term}%"
    from j in query,
         where: like(j.name, ^search_term) or like(j.pdb_id, ^search_term)
  end

  def show_job(id) do
    Repo.get(Job, id)
  end

  def create_job(attrs \\ %{}) do
    job = Job.create_job_changeset(%Job{}, attrs)
    Repo.insert(job)
  end

  @spec run_task(String.t(), String.t(), (-> :ok) | (-> {:error, String.t()})) :: Job
  def run_task(pdb_id, job_name, task) do
    with {:ok, job} <- create_job(%{name: job_name, pdb_id: pdb_id}) do
      do_run(job, task)
      {:ok, job}
    end
  end

  defp do_run(job, task) do
    wrapped_task = fn ->
      try do
        case task.(job) do
          :ok ->
            finish_job_with_success(job.id)
          error ->
            finish_job_with_error(job.id, RunError.map(error))
        end
      rescue
        e ->
          finish_job_with_error(job.id, RunError.map(e))
      end
    end

    Task.Supervisor.async(Colmena.TaskSupervisor, wrapped_task)
  end

  defp finish_job_with_error(id, message) do
    job = Repo.get(Job, id)
    finish_job(job, Job.error(), message)
  end

  defp finish_job_with_success(id) do
    job = Repo.get(Job, id)
    finish_job(job, Job.success())
  end

  defp finish_job(job, state, msg \\ "") do
    job = Job.update_job_changeset(job, state, DateTime.utc_now(), msg)
    Repo.update(job)
  end
end
