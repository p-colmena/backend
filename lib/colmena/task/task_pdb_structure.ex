defmodule Colmena.Task.TaskPdbStructure do
  alias Colmena.Process.Job
  alias Colmena.PdbProcessor.PdbStructure

  use Ecto.Schema
  import Ecto.Changeset

  schema "task_pdb_structures" do
    belongs_to :job, Job
    belongs_to :pdb_structure, PdbStructure
    timestamps()
  end

  def create_changeset(task_pdb_structure, attrs \\ %{}) do
    task_pdb_structure
    |> cast(attrs, [:job_id, :pdb_structure_id])
    |> assoc_constraint(:job)
    |> assoc_constraint(:pdb_structure)
    |> validate_required([:job_id, :pdb_structure_id])
    |> unique_constraint(:job_id)
  end
end
