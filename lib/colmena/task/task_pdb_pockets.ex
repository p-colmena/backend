defmodule Colmena.Task.TaskPdbPockets do
  alias Colmena.Task.TaskPdbStructure
  alias Colmena.PdbProcessor.PdbPockets

  use Ecto.Schema
  import Ecto.Changeset

  schema "task_pdb_pockets" do
    belongs_to :task_pdb_structure, TaskPdbStructure
    belongs_to :pdb_pockets, PdbPockets
    timestamps()
  end

  def create_changeset(task_pdb_pockets, attrs \\ %{}) do
    task_pdb_pockets
    |> cast(attrs, [:task_pdb_structure_id, :pdb_pockets_id])
    |> assoc_constraint(:task_pdb_structure)
    |> assoc_constraint(:pdb_pockets)
    |> validate_required([:task_pdb_structure_id, :pdb_pockets_id])
    |> unique_constraint(:task_pdb_structure_id)
  end
end
