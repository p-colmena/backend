defmodule Colmena.Task.TaskPdbSurface do
  alias Colmena.Task.TaskPdbStructure
  alias Colmena.PdbProcessor.PdbSurface

  use Ecto.Schema
  import Ecto.Changeset

  schema "task_pdb_surfaces" do
    belongs_to :task_pdb_structure, TaskPdbStructure
    belongs_to :pdb_surface, PdbSurface
    timestamps()
  end

  def create_changeset(task_pdb_surface, attrs \\ %{}) do
    task_pdb_surface
    |> cast(attrs, [:task_pdb_structure_id, :pdb_surface_id])
    |> assoc_constraint(:task_pdb_structure)
    |> assoc_constraint(:pdb_surface)
    |> validate_required([:task_pdb_structure_id, :pdb_surface_id])
    |> unique_constraint(:task_pdb_structure_id)
  end
end
