defmodule Colmena.Task do
  alias Colmena.Repo
  alias Colmena.Task.TaskPdbStructure
  alias Colmena.PdbProcessor
  alias Colmena.Task.TaskPdbSurface
  alias Colmena.Task.TaskPdbPockets

  import Ecto.Query, only: [from: 2]
  
  ################## CREATON ##################
  @spec pdb_structure(Job, String.t()) :: {:ok, TaskPdbStructure} | {:error, any()}
  def pdb_structure(job, pdb_id) do
    with {:ok, structure} <- PdbProcessor.save(pdb_id) do
      create_task_pdb_structure(job, structure)
    end
  end

  @spec pdb_surface(TaskPdbStructure) :: {:ok, TaskPdbSurface} | {:error, any()}
  def pdb_surface(task_pdb_structure) do
    with {:ok, obtained_surface} <- PdbProcessor.surface(task_pdb_structure.pdb_structure) do
      create_task_pdb_surface(task_pdb_structure, obtained_surface)
    end
  end

  @spec pdb_pockets(TaskPdbStructure) :: {:ok, TaskPdbPockets} | {:error, any()}
  def pdb_pockets(task_pdb_structure) do
    with {:ok, pdb_pockets} <- PdbProcessor.pockets(task_pdb_structure.pdb_structure) do
      create_task_pdb_pockets(task_pdb_structure, pdb_pockets)
    end
  end

  defp create_task_pdb_pockets(task_pdb_structure, pdb_pockets) do
    with task_pdb_pockets_attrs <- %{task_pdb_structure_id: task_pdb_structure.id, pdb_pockets_id: pdb_pockets.id},
         changeset <- TaskPdbPockets.create_changeset(%TaskPdbPockets{}, task_pdb_pockets_attrs),
         {:ok, task} <- Repo.insert(changeset) do
      {:ok, Repo.preload(task, [:task_pdb_structure, :pdb_pockets])}
    end
  end

  defp create_task_pdb_surface(task_pdb_structure, pdb_surface) do
    with task_pdb_surface_map <- %{task_pdb_structure_id: task_pdb_structure.id, pdb_surface_id: pdb_surface.id},
         changeset <- TaskPdbSurface.create_changeset(%TaskPdbSurface{}, task_pdb_surface_map),
         {:ok, task} <- Repo.insert(changeset) do
      {:ok, Repo.preload(task, [:task_pdb_structure, :pdb_surface])}
    end
  end

  defp create_task_pdb_structure(job, pdb_structure) do
    with task_pdb_structure_map <- %{job_id: job.id, pdb_structure_id: pdb_structure.id},
         task_changeset <- TaskPdbStructure.create_changeset(%TaskPdbStructure{}, task_pdb_structure_map),
         {:ok, task} <- Repo.insert(task_changeset) do
      {:ok, Repo.preload(task, [:job, :pdb_structure])}
    end
  end

  ################## SHOW ##################
  def show_task_pdb_structure(pdb_id) do
    query = from t in TaskPdbStructure,
         join: p in assoc(t, :pdb_structure),
         where: p.pdb_id == ^pdb_id,
         preload: [:job, :pdb_structure]

    task = List.first(Repo.all(query))

    case task do
      nil -> {:error, :not_found}
      task -> {:ok, task}
    end
  end

  def show_task_pdb_surface(task_pdb_structure) do
    task = Repo.get_by(TaskPdbSurface, task_pdb_structure_id: task_pdb_structure.id)
           |> Repo.preload([:task_pdb_structure, :pdb_surface])

    case task do
      nil -> {:error, :not_found}
      some -> {:ok, some}
    end
  end

  def show_task_pdb_pockets(task_pdb_structure) do
    task = Repo.get_by(TaskPdbPockets, task_pdb_structure_id: task_pdb_structure.id)
           |> Repo.preload([:task_pdb_structure, pdb_pockets: :pockets])

    case task do
      nil -> {:error, :not_found}
      some -> {:ok, some}
    end
  end

  def show_tasks(pdb_id) do
    with {:ok, task_structure} <- show_task_pdb_structure(pdb_id) do
      case {show_task_pdb_surface(task_structure), show_task_pdb_pockets(task_structure)} do
        {{:ok, task_surface}, {:ok, task_pockets}} ->
          {:ok, [{:structure, task_structure}, {:surface, task_surface}, {:pockets, task_pockets}]}
        {{:error, _}, {:ok, task_pockets}} ->
          {:ok, [{:structure, task_structure}, {:pockets, task_pockets}]}
        {{:ok, task_surface}, {:error, _}} ->
          {:ok, [{:structure, task_structure}, {:surface, task_surface}]}
        {{:error, _}, {:error, _}} ->
          {:error, :not_found}
      end
    end
  end
end