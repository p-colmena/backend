defmodule ColmenaWeb do
  def controller do
    quote do
      use Phoenix.Controller, namespace: ColmenaWeb
      import Plug.Conn
      import ColmenaWeb.Router.Helpers
      import ColmenaWeb.Gettext
    end
  end

  def view do
    quote do
      use Phoenix.View,
          root: "lib/colmena_web/templates",
          namespace: ColmenaWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      import ColmenaWeb.Router.Helpers
      import ColmenaWeb.ErrorHelpers
      import ColmenaWeb.Gettext
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
      use Plug.ErrorHandler
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import ColmenaWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
