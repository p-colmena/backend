defmodule ColmenaWeb.JobControllerTest do
  use ColmenaWeb.ConnCase
  alias Colmena.Process
  alias Colmena.PdbProcessor.Helper

  @create_attrs %{name: "a job", pdb_id: "pdb id"}

  def fixture(:job) do
    {:ok, job} = Process.create_job(@create_attrs)
    job
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all jobs", %{conn: conn} do
      job = fixture(:job)

      conn = get conn, job_path(conn, :index), %{searchTerm: ""}
      assert json_response(conn, 200)["jobs"] ==
               [
                 %{
                   "id" => job.id,
                   "name" => job.name,
                   "state" => job.state,
                   "result" => nil,
                   "pdbId" => job.pdb_id,
                   "duration" => nil
                 }
               ]
    end
  end
end
