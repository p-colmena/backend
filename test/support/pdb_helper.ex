ExUnit.start()

defmodule Colmena.PdbProcessor.Helper do

  use ExUnit.Case
  import Tesla.Mock

  def mock_response_with(status, body \\ "") do
    mock fn
      %{method: :get, url: "https://files.rcsb.org/view/pdb.pdb"} -> %Tesla.Env{status: status, body: body}
    end
  end

  def assert_file_content(file, expected_content) do
    file_content = File.read!(base_path() <> file)
    assert file_content == expected_content
  end

  def assert_file_not_exist(file) do
    assert {:error, :enoent} = File.read(base_path() <> file)
  end

  def base_path do
    Path.expand(Application.get_env(:colmena, Colmena.PDB)[:base_path])
  end

  def clean_base_pdb_path do
    files = File.ls! base_path()
    files = Enum.filter(files, fn file -> file != ".gitkeep" end)
    Enum.each(files, fn file -> File.rm_rf!(base_path() <> "/" <> file) end)
  end
end