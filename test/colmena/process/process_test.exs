defmodule Colmena.ProcessTest do
  use Colmena.DataCase
  alias Colmena.Process, as: ColmenaProcess
  alias Colmena.Process.Job

  describe "jobs" do

    @job_attrs %{name: "a job", pdb_id: "a pdb id"}

    def job_fixture(attrs \\ %{}) do
      {:ok, job} = ColmenaProcess.create_job(Enum.into(@job_attrs, attrs))
      job
    end

    test "create_job/1 with valid data creates a job" do
      {:ok, %Job{} = job} = ColmenaProcess.create_job(@job_attrs)

      assert "running" = job.state
      assert "a job" = job.name
      assert nil != job.started_at
      assert nil == job.finished_at
    end

    test "create_job/1 without name return error changeset" do
      attrs = Map.put(@job_attrs, :name, nil)
      assert {:error, %Ecto.Changeset{}} = ColmenaProcess.create_job(attrs)
    end
  end

  describe "task execution" do
    test "run a task creates a job with running with the name" do
      {:ok, %Job{} = job} = ColmenaProcess.run_task("pdb id", "a task", fn -> IO.puts "a task" end)

      assert "a task" = job.name
    end

    test "finished task is in success state" do
      pid = self()
      task = fn (_) ->
        send pid, :excecuted
        :ok
      end

      {:ok, %Job{} = job} = ColmenaProcess.run_task("pdb id", "a task", task)

      assert_receive :excecuted
      Process.sleep(200)

      job = ColmenaProcess.show_job(job.id)
      assert true = Job.finished_with_success(job)
    end

    test "check if the job is running" do
      task = fn () ->
        Process.sleep(1000)
      end

      {:ok, %Job{} = job} = ColmenaProcess.run_task("pdb id", "a task", task)

      assert true = Job.is_running(job)
    end

    test "if exception throws in task the job finish with error" do
      pid = self()
      task = fn (_) ->
        send pid, :excecuted
        raise "An error"
      end

      {:ok, %Job{} = job} = ColmenaProcess.run_task("pdb id", "a task", task)

      assert_receive :excecuted
      Process.sleep(100)

      job = ColmenaProcess.show_job(job.id)
      assert true = Job.finished_with_error(job)
    end

    test "if task returns error the the job finish with error" do
      pid = self()
      task = fn (_) ->
        send pid, :excecuted
        {:error, "An error"}
      end

      {:ok, %Job{} = job} = ColmenaProcess.run_task("pdb id", "a task", task)

      assert_receive :excecuted
      Process.sleep(100)

      job = ColmenaProcess.show_job(job.id)
      assert true = Job.finished_with_error(job)
    end
  end
end
