defmodule Colmena.PdbProcessor.FileTest do
  use ExUnit.Case

  alias Colmena.PdbProcessor.File, as: ColmenaFiles
  alias Colmena.PdbProcessor.Helper, as: Helper

  setup do
    on_exit fn ->
      Helper.clean_base_pdb_path()
    end
  end

  describe "pdb files" do
    test "write a file" do
      assert :ok = ColmenaFiles.save_file("", "file", "my file content")
      Helper.assert_file_content "/file", "my file content"
    end

    test "if the sub path does not exists creates it" do
      assert :ok = ColmenaFiles.save_file("/subpath", "file", "my file content")
      Helper.assert_file_content("/subpath/file", "my file content")
    end

    test "if the sub path does exists writes the file in it" do
      path = "/my/sub/path"
      assert :ok = File.mkdir_p(Helper.base_path() <> path)
      assert :ok = ColmenaFiles.save_file(path, "file", "my file content")
      Helper.assert_file_content(path <> "/file", "my file content")
    end
  end
end

defmodule Colmena.PdbProcessor.HttpTest do
  use ExUnit.Case
  alias Colmena.PdbProcessor.Http
  alias Colmena.PdbProcessor.Helper, as: Helper

  describe "get pbd" do
    test "with success if response is 200" do
      Helper.mock_response_with(200, "the body")
      assert {:ok, response} = Http.get_pdb("pdb.pdb")
      assert "the body" = response
    end

    test "with error if response is anithing else than 200" do
      Helper.mock_response_with 400
      assert {:error, :response_status_not_ok, 400} = Http.get_pdb("pdb.pdb")
    end
  end
end

defmodule Colmena.PdbProcessorTest do
  use ExUnit.Case
  use Colmena.DataCase

  alias Colmena.PdbProcessor
  alias Colmena.PdbProcessor.Helper, as: Helper
  alias Colmena.PdbProcessor.PdbStructure

  setup do
    on_exit fn ->
      Helper.clean_base_pdb_path()
    end
  end

  describe "save a pdb structure" do
    test "if pdb service response fail" do
      Helper.mock_response_with(400, "")
      assert {:error, :response_status_not_ok, 400} = PdbProcessor.save("pdb")
    end

    test "save file with success" do
      Helper.mock_response_with(200, "lalal")
      {:ok, pdb_structure} = PdbProcessor.save("pdb")

      {:ok, structure} = PdbProcessor.show_pdb_structure(pdb_structure.id)

      assert pdb_structure.id == structure.id
      assert pdb_structure.pdb_id == structure.pdb_id
      assert pdb_structure.structure == structure.structure
    end
  end

  describe "create a pdb structure" do
    test "cant create without pdb_id" do
      assert {
               :error,
               %Ecto.Changeset{
                 errors: [
                   pdb_id: _
                 ]
               }
             } = PdbProcessor.create_pdb_structure(%{structure: "lala"})
    end

    test "cant create with nil pdb_id" do
      assert {
               :error,
               %Ecto.Changeset{
                 errors: [
                   pdb_id: _
                 ]
               }
             } = PdbProcessor.create_pdb_structure(%{pdb_id: nil, structure: "lala"})
    end

    test "cant create without structure" do
      assert {
               :error,
               %Ecto.Changeset{
                 errors: [
                   structure: _
                 ]
               }
             } = PdbProcessor.create_pdb_structure(%{pdb_id: "lala"})
    end

    test "cant create with nil structure" do
      assert {
               :error,
               %Ecto.Changeset{
                 errors: [
                   structure: _
                 ]
               }
             } = PdbProcessor.create_pdb_structure(%{pdb_id: "lala", structure: nil})
    end

    test "create a pdb_structure" do
      assert {
               :ok,
               %PdbStructure{
                 id: _,
                 pdb_id: "lala",
                 structure: "lelele",
                 inserted_at: _,
                 updated_at: _
               }
             } = PdbProcessor.create_pdb_structure(%{pdb_id: "lala", structure: "lelele"})
    end
  end
end