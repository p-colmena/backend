# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :colmena, Colmena.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "colmena_repo",
  username: "user",
  password: "pass",
  hostname: "localhost"


# General application configuration
config :colmena,
  ecto_repos: [Colmena.Repo]

# Configures the endpoint
config :colmena, ColmenaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "xBNslBUnNSDFIsS5KZDv4wJVGcX4/Kx1JiUZ3TSIUNaYpM97JXkk4WhJqe7kedJJ",
  render_errors: [view: ColmenaWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Colmena.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
