use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :colmena,
       ColmenaWeb.Endpoint,
       http: [
         port: 4001
       ],
       server: false

# Print only warnings and errors during test
config :logger, level: :info

# Configure your database
config :colmena,
       Colmena.Repo,
       adapter: Ecto.Adapters.Postgres,

       username: System.get_env("POSTGRESQL_USER") || "postgres",
       password: System.get_env("POSTGRESQL_PASS") || "postgres",
       database: System.get_env("POSTGRES_DB") || "colmena_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",

       pool: Ecto.Adapters.SQL.Sandbox

config :colmena, Colmena.PDB,
       base_path: "./.colmena/pdb/"

config :tesla, Colmena.PdbProcessor.Http, adapter: Tesla.Mock