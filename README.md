# Colmena

Prerequisites:
   * [Elixir](https://elixir-lang.org/install.html) 1.4 or higher
   * [Mix](https://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html) 1.7.3 or higher
   * [Postgres](https://www.postgresql.org/)
   * [FreeSASA](https://freesasa.github.io/) in your path
   * [FPocket](https://github.com/Discngine/fpocket) in your path
   
To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you have the rest api running on [`localhost:4000`](http://localhost:4000)

For more information you can see the [documentation](https://docs.google.com/document/d/1FZBeXmIG8dGlXdgIiYVwPt2Lf5aR7wmAHxWjxlbHyP0/edit?usp=sharing) or contact me [garciasmithsagustin@gmail.com](garciasmithagustin@gmail.com)