defmodule Colmena.Repo.Migrations.PdbIdUnique do
  use Ecto.Migration

  def change do
    create unique_index(:pdb_structures, [:pdb_id])
  end
end
