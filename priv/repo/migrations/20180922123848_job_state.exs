defmodule Colmena.Repo.Migrations.JobState do
  use Ecto.Migration

  def change do
    drop index("jobs", [:pid])
    alter table("jobs") do
      remove :pid
      remove :inserted_at
      remove :updated_at

      add :state, :string, size: 10, null: false
      add :name, :string, size: 60, null: false
      add :started_at, :naive_datetime, default: NaiveDateTime.to_string(NaiveDateTime.utc_now()), null: false
      add :finished_at, :naive_datetime
    end
  end
end
