defmodule Colmena.Repo.Migrations.AddPdb do
  use Ecto.Migration

  def change do
    alter table("jobs") do
      add :pdb_id, :string, size: 20, null: false
    end
  end
end
