defmodule Colmena.Repo.Migrations.AddResult do
  use Ecto.Migration

  def change do
    alter table("jobs") do
      add :result, :string, size: 400
    end
  end
end