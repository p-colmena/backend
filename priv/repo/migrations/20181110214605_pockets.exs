defmodule Colmena.Repo.Migrations.Pockets do
  use Ecto.Migration

  def change do
    alter table("jobs") do
      add :pdb_id, :string, size: 10, null: false
    end

    create table(:pdb_pockets) do
      add :pdb_id, :string, size: 10, null: false
      add :info, :binary, null: false
      add :pdb, :binary, null: false
      add :zip, :binary, null: false
      timestamps()
    end
    create unique_index(:pdb_pockets, [:pdb_id])

    create table(:pockets) do
      add :pdb_id, :string, size: 10, null: false
      add :structure, :binary, null: false
      add :pdb_pockets_id, references(:pdb_pockets)
      timestamps()
    end

    create table(:task_pdb_pockets) do
      add :task_pdb_structure_id, references(:task_pdb_structures)
      add :pdb_pockets_id, references(:pdb_pockets)
      timestamps()
    end
    create unique_index(:task_pdb_pockets, [:task_pdb_structure_id])
  end
end
