defmodule Colmena.Repo.Migrations.CreatePdbStructure do
  use Ecto.Migration

  def change do
    create table(:pdb_structures) do
      add :pdb_id, :string, size: 10, null: false
      add :structure, :binary, null: false
      timestamps()
    end
  end
end
