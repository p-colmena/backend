defmodule Colmena.Repo.Migrations.PocTable do
  use Ecto.Migration

  def change do
    create table(:poc_table) do
      add(:title, :string, size: 40, default: "Hello", null: false)
      timestamps()
    end
  end
end
