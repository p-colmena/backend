defmodule Colmena.Repo.Migrations.DeletePdbId do
  use Ecto.Migration

  def change do
    alter table("jobs") do
      remove :pdb_id
    end
  end
end
