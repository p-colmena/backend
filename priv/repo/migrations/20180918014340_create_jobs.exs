defmodule Colmena.Repo.Migrations.CreateJobs do
  use Ecto.Migration

  def change do
    create table(:jobs) do
      add :pid, :string
      timestamps()
    end
    create index("jobs", [:pid], unique: true)
  end
end
