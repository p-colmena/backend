defmodule Colmena.Repo.Migrations.TaskGetPdbStructure do
  use Ecto.Migration

  def change do
    create table(:pdb_surfaces) do
        add :pdb_id, :string, size: 10, null: false
        add :structure, :binary, null: false
        timestamps()
    end
    create unique_index(:pdb_surfaces, [:pdb_id])

    create table(:task_pdb_structures) do
      add :job_id, references(:jobs)
      add :pdb_structure_id, references(:pdb_structures)
      timestamps()
    end
    create unique_index(:task_pdb_structures, [:job_id])

    create table(:task_pdb_surfaces) do
      add :task_pdb_structure_id, references(:task_pdb_structures)
      add :pdb_surface_id, references(:pdb_surfaces)
      timestamps()
    end
    create unique_index(:task_pdb_surfaces, [:task_pdb_structure_id])
  end
end
